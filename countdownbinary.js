COUNTDOWN = {

   x : {
    1: [1,0,0,0],
    2: [0,1,0,0],
    3: [1,1,0,0],
    4: [0,0,1,0],
    5: [1,0,1,0],
    6: [0,1,1,0],
    7: [1,1,1,0],
    8: [0,0,0,1],
    9: [1,0,0,1],
    0: [0,0,0,0]
  },

  div: ["h","hh", "m", "mm","s","ss"],

  minutes: 0,
  secondes: 0,
  togo: 0,
  count: "",


  Init: function(){
    document.getElementById('minutes').addEventListener("focusout", COUNTDOWN.SetMinutes);
    document.getElementById('secondes').addEventListener("focusout", COUNTDOWN.SetSecondes);
    document.getElementById('reset').addEventListener("click",COUNTDOWN.Reset);
    document.getElementById('start').addEventListener("click", COUNTDOWN.Start);
  },

  SetMinutes: function(evt){
    COUNTDOWN.minutes = evt.target.value * 60;
  },

  SetSecondes: function(evt){
    COUNTDOWN.secondes = parseInt(evt.target.value);
  },

  Reset: function(){
    try{
      document.getElementById('minutes').value = "0";
      document.getElementById('secondes').value = "0";
      COUNTDOWN.minutes = 0;
      COUNTDOWN.secondes = 0;
      COUNTDOWN.togo = 0;
      document.getElementById('pause').removeEventListener("click", COUNTDOWN.Pause);
      document.getElementById('pause').id = "start";
      document.getElementById('start').addEventListener("click", COUNTDOWN.Start);
      document.getElementById('start').innerHTML = "Start";
      COUNTDOWN.ChangeToTime();
    } catch(error) {}
  },

  Start: function(){
    COUNTDOWN.togo = COUNTDOWN.minutes + COUNTDOWN.secondes;
    if (COUNTDOWN.togo > 0){
      document.getElementById('start').removeEventListener("click",COUNTDOWN.Start);
      document.getElementById('start').id = "pause";
      document.getElementById('pause').innerHTML = "pause";
      document.getElementById('pause').addEventListener("click",COUNTDOWN.Pause);
      COUNTDOWN.ChangeToTime();
    }
  },

  ChangeToTime: function(){
    hours = Math.floor((COUNTDOWN.togo / 3600) % 60 );
    minutes = Math.floor((COUNTDOWN.togo / 60) % 60 ); //get minutes
    secondes = COUNTDOWN.togo % 60;  // get secondes

    // split hours
    dix_de_hour = Math.floor( hours / 10);
    hour = hours % 10;

    //split minutes
    dix_de_min = Math.floor( minutes / 10);
    minute = minutes % 10;

    //split secondes
    dix_de_sec = Math.floor( secondes / 10); // bon pour les dizaine de secondes
    secs = secondes % 10;


    total = [dix_de_hour, hour, dix_de_min,minute,dix_de_sec,secs];
    COUNTDOWN.Display(total);
  },

  Display: function(ti){
    for (i=0; i < ti.length; i++){
      maindiv = COUNTDOWN.div[i];
      divcolor = COUNTDOWN.x[ti[i]];
      for (j=0; j < divcolor.length; j++){
        timeColor = divcolor[j] == 1 ? "#808080" :  "#ffffff";

        if (document.getElementById(maindiv + j)){
          document.getElementById(maindiv + j).style.backgroundColor = timeColor;
        }
      }//end second FOR
    }//end first for
    COUNTDOWN.CountDown();
  },

  CountDown: function(){
    COUNTDOWN.togo -= 1;
    if (COUNTDOWN.togo >= 0){
      COUNTDOWN.countdown = setTimeout(COUNTDOWN.ChangeToTime, 1000);
    } else {
      clearTimeout(COUNTDOWN.countdown);
    }
  },

  Pause: function(){
    if (COUNTDOWN.countdown){
      clearTimeout(COUNTDOWN.countdown);
      COUNTDOWN.countdown = null;
      document.getElementById('pause').innerHTML = "restart";
    } else {
      document.getElementById('pause').innerHTML = "pause";
      COUNTDOWN.countdown = setTimeout(COUNTDOWN.ChangeToTime, 1000);
    }
  }
};
COUNTDOWN.Init();
